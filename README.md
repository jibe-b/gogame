# Une trame de jeux entre deux Grandes Opérations

Voici une proposition de trame de jeu entre deux Grandes Opérations à Strasbourg.

## Ligne éditoriale

Cette mécanique de jeu peut être utilisée comme moyen pour faire connaître les effets des choix qui pourraient être pris par un conseil municipal.

À une décision, un jeu est associé (0. décision municipale).

Par exemple, la décision "plus de voiture en vile" permettrait "agriculture urbaine".

## Mécaniques de jeu

Deux mécaniques de jeu sont associées :

- une collecte d'éléments dans la ville (en extérieur)
- un jeu de simulation à partir des objets collectés (en intérieur)

### Partie 1 : collecte d'éléments

La première mécanique de jeu demande de collecter ou créer à partir d'éléments présents en extérieur.

Les tâches à réaliser sont attribués par paquets, à réaliser dans un temps possiblement limité.

L'ordre dans lequel réaliser cette série de tâches est un défi (2. défi logistique) : son optimisation donne des points.

Une fois les tâches d'une série réalisées (1. défis élémentaires), elles sont poussées vers le serveur qui les prendra en compte pour la suite.

Une énigme sur le thème du jeu peut être résolue à partir des tâches réalisées (3. énigme).

### Partie 2 : jeu de simulation

Le jeu de simulation utilise les objets et fonctionnalités collectées en extérieur.

La simulation doit être paramétrée pour une simulation qui va être exécutée le temps du jeu (4. défi paramétrisation). Ce défi a pour but d'inciter à réfléchir sur ce que signifie établir des politiques publiques.


Les paramétrisations des différentes équipes vont être superposées et l'interaction positive entre les paramétrisations va être évaluée (5. défi interaction). Cela a pour but d'inciter les équipes à échanger entre elles et s'organiser pour assurer des interactions positives au sein de la simulation.

## Critères du jeu

Ce jeu est une manière ludique d'introduire :

- l'acquisition de données sur un territoire
- le maintien de ces données hors du copyright en les mettant disponibles sous licence libre
- aux politiques publiques (par thématique)
- aux politiques publiques (en voyant l'effet d'une politique comme par exemple "plus de voiture en ville", permettant par exemple l'agriculture urbaine)



## Trame générale

Jeu | Décision municipale | Défis élémentaires | Défi logistique | Énigme | Défi Paramétrisation | éfiinteraction |
-|-|-|-|-|-|-|
Espaces de sport | Casiers et douches gratuites | création de parcours de sport | suivi des parcours |  | flux de sportifs simulés | interaction entre flux |
Tou·te·s chercheu·se·r·s | Espace voiture –> espace agriculture urbaine | Photos de plantes | Ordre des photos | Usage de plantes | Simulation culture plantes | Commerce de plantes (simulation) |

## Espaces de sport

Designer des parcours de sport à partir de ce qui est présent dans la ville.

0. Décision municipale

La ville assure casiers et douches gratuitement.

1. Défis élémentaires (multiples)

Créer des parcours

exemple :

- utiliser casiers à tel endroit
- suivre tel parcours de sport en courant
- enchainer avec de la musculation dans tel parc
- utiliser douche à tel endroit
- points étapes (photo à prendre à tel endroit)

2. Défi logistique

Suivre ces parcours en équipe (avec des photos d'étapes pour valider). Faire que ce parcours soit le plus populaire.

Suivre des parcours des autres équipes, interagir avec ces parcours.

3. Énigme

4. Défi paramétrisation

À partir des parcours, paramétrisation d'un flux de sportifs simulés

5. Défi interaction

Interaction entre les flux.



## Tou·te·s chercheu·se·r·s

0. Décision municipale

L'espace pour les voitures est dédié pour 70% à l'agriculture urbaine.

1. Défis élémentaires

Photographier des plantes dans des zones.

Photographier des infrastructures pour la culture de plantes (par exemple une fontaine).

2. Défi logistique

Optimisation du parcours pour réaliser chacune des actions en 6 heures.

3. Énigme

Usage possible de ces plantes.

4. Défi paramétrisation

Paramétrisation d'un plan de culture de plantes (maraichage urbain).

5. Défi interaction

Interaction entre les différentes simulations correspondant à du commerce.



## Jeu

1. Défis élémentaires

2. Défi logistique

3. Énigme

4. Défi paramétrisation

5. Défi interaction


